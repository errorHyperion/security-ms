const GroupController = module.exports;

const log4j = require('../utils/logger');
const logUtils = require('../utils/LogUtils');
const GroupService = require('../services/groupService');
const UserService = require('../services/userService');

GroupController.addPermissionsToGroup = (req, res, next) => {
  const logName = 'addPermissionsToGroup';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params, body } = req;
  logger.info(`Starts GroupController.addPermissionsToGroup: ${JSON.stringify(params)}, ${JSON.stringify(body)}`);

  return GroupService.addPermissionsToGroup(params.name, body.permissions, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in GroupController.addPermissionsToGroup: ${error.message}`);

      return next(error);
    });
};

GroupController.removePermissionsToGroup = (req, res, next) => {
  const logName = 'removePermissionsToGroup';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params, body } = req;
  logger.info(`Starts GroupController.removePermissionsToGroup: ${JSON.stringify(params)}, ${JSON.stringify(body)}`);

  return GroupService.removePermissionsToGroup(params.name, body.permissions, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in GroupController.removePermissionsToGroup: ${error.message}`);

      return next(error);
    });
};

GroupController.deleteGroup = (req, res, next) => {
  const logName = 'Delete Group';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts GroupController.DeleteGroup: ${JSON.stringify(params)}`);

  return GroupService.deleteGroup(params.groupname, { logger, logName })
    .then((response) => res.send(response)).catch((error) => {
      logger.error(`Error in GroupController.DeleteGroup: ${error.message}`);

      return next(error);
    });
};

GroupController.listUsersFromGroup = (req, res, next) => {
  const logName = 'listUsersFromGroup';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts UserController.listUsersFromGroup: ${JSON.stringify(params)}`);

  return UserService.listUsersFromGroup(params.groupname, { logger, logName })
    .then((response) => res.send(response)).catch((error) => {
      logger.error(`Error in UserController.listUsersFromGroup: ${error.message}`);

      return next(error);
    });
};

GroupController.createGroup = (req, res, next) => {
  const logName = 'createGroup';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info(`Starts GroupController.createGroup : ${JSON.stringify(body)}`);

  return GroupService.createGroup(body, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in GroupController.createGroup : ${error.message}`);

      return next(error);
    });
};

GroupController.listGroups = (req, res, next) => {
  const logName = 'listGroups';
  const logger = logUtils.getLoggerWithId(log4j, logName);

  logger.info(`Starts GroupController.listGroups: ${JSON.stringify}`);

  return GroupService.listGroups({ logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in GroupController.listGroups: ${error.message}`);

      return next(error);
    });
};
