const express = require('express');
const PermissionController = require('./permissionController');
const GroupController = require('./groupController');
const UserController = require('./userController');
const SecurityController = require('./securityController');

const router = express.Router();

router.put('/groups/:name/permissions', GroupController.addPermissionsToGroup);
router.delete('/groups/:groupname', GroupController.deleteGroup);
router.delete('/groups/:name/permissions', GroupController.removePermissionsToGroup);
router.get('/groups/:groupname/users', GroupController.listUsersFromGroup);
router.post('/groups', GroupController.createGroup);
router.get('/groups', GroupController.listGroups);

router.post('/users', UserController.create);
router.put('/users/:username/update/groups/:name', UserController.updateGroup);
router.put('/users/:username/change-password', UserController.changePassword);
router.get('/users/:username/authorizations', UserController.findUserPermissionsByUsername);
router.delete('/users/:code/permission/:name', UserController.removePermission);

router.post('/permissions', PermissionController.create);

router.put('/users/:username/permissions', UserController.addPermissionsToUser);
router.delete('/users/:username/groups', UserController.removeGroupsToUser);

router.put('/user/:username/activate', UserController.activate);
router.put('/user/:username/desactivate', UserController.desactivate);

router.post('/security/login', SecurityController.login);
router.post('/security/validate-token', SecurityController.validateToken);

const permissionController = require('./permissionController');

router.get('/permissions/:name', permissionController.list);
router.delete('/permissions/:name', permissionController.delete);
module.exports = router;
