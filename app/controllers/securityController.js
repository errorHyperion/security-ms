const SecurityController = module.exports;

const log4j = require('../utils/logger');
const logUtils = require('../utils/LogUtils');
const securityService = require('../services/securityService');

SecurityController.login = (req, res, next) => {
  const logName = 'login';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info('Starts SecurityController.login');

  return securityService.login(body.username, body.pass, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in SecurityController.login: ${error.message}`);

      return next(error);
    });
};

SecurityController.validateToken = (req, res, next) => {
  const logName = 'validate token';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info('Starts SecurityController.validateToken');

  return securityService.validateToken(body.token, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in SecurityController.validateToken: ${error.message}`);

      return next(error);
    });
};
