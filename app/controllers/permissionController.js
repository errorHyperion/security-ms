const permissionController = module.exports;
const log4j = require('../utils/logger');
const logUtils = require('../utils/LogUtils');
const permissionService = require('../services/permissionService');

permissionController.create = (req, res, next) => {
  const logName = 'createPermission';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info(`Starts PermissionController.create: ${JSON.stringify(body)}`);

  return permissionService.create(body, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in PermissionController.create: ${error.message}`);

      return next(error);
    });
};
permissionController.list = (req, res, next) => {
  const logName = 'listPermission';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts permissionController.list : ${JSON.stringify(params)}`);

  return permissionService.list(params.name, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in permissionController.list : ${error.message}`);

      return next(error);
    });
};

permissionController.delete = (req, res, next) => {
  const logName = 'deletePermission';
  const logger = logUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts permissionController.delete : ${JSON.stringify(params)}`);

  return permissionService.delete(params.name, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in permissionController.delete : ${error.message}`);

      return next(error);
    });
};
