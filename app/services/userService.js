const UserService = module.exports;
// const randomstring = require('randomstring');
const emailRegex = require('email-regex');

const Encrypter = require('../utils/encrypter');

const { NotFoundError, BusinessError, PartialContentError } = require('../utils/ErrorHandlerMiddleware');

const UserRepository = require('../repositories/userRepository');
const GroupRepository = require('../repositories/groupRepository');
const PermissionRepository = require('../repositories/permissionRepository');

UserService.create = async (user) => {
  const userToFind = (await UserRepository.findByUsername(user.username))[0];

  if (userToFind) {
    throw new BusinessError('Username is repeated.');
  }

  if (!user.email.match(emailRegex())) {
    throw new BusinessError('Email is not valid.');
  }

  const validUntil = new Date();
  validUntil.setDate(validUntil.getDate() + 30);

  // const userPassword = randomstring.generate(8);

  const passwords = [{
    password: Encrypter.encryptPassword(user.email),
    createdAt: new Date(),
    validUntil,
    isCurrent: true,
  }];

  const userToSave = {
    passwords,
    username: user.username,
    email: user.email,
    mustChangePassword: true,
    isActive: true,
  };

  await UserRepository.create(userToSave);
};

UserService.addGroupToList = async (username, groupName) => {
  const userToFind = (await UserRepository.findByUsername(username))[0];

  if (!userToFind) {
    throw new NotFoundError('User does not exist.');
  }

  const groupToFind = (await GroupRepository.findByName(groupName))[0];

  if (!groupToFind) {
    throw new NotFoundError('There is not a group with this name.');
  }

  const findUserGroup = (await UserRepository.findUserGroupByName(groupName))[0];

  if (findUserGroup) {
    throw new BusinessError('User has this group already.');
  }

  userToFind.groups.push(groupName);

  await UserRepository.edit(userToFind);
};

UserService.changePassword = async (username, currentPassword, newPassword) => {
  const user = (await UserRepository.findByUsername(username))[0];

  if (!user) {
    throw new NotFoundError('User not found');
  }

  if (newPassword.length < 8) {
    throw new BusinessError('The password must be at least 8 characters long');
  }

  if (!await Encrypter.comparePassword(currentPassword, user.passwords[0].password)) {
    throw new BusinessError('The current password is incorrect');
  } else {
    user.passwords[0].isCurrent = false;
  }

  if (user.passwords.length === 10) {
    user.passwords.pop();
  }

  const passwordComparision = await Promise.all(user.passwords.map(async (password) => {
    const isEqual = await Encrypter.comparePassword(newPassword, password.password);

    return isEqual;
  }));

  if (passwordComparision.some((it) => it === true)) {
    throw new BusinessError('The new password has already been used before');
  }

  const validUntil = new Date();

  validUntil.setDate(validUntil.getDate() + 30);

  user.passwords.unshift({
    password: Encrypter.encryptPassword(newPassword),
    createdAt: new Date(),
    validUntil,
    isCurrent: true,
  });

  await UserRepository.edit(user);
};

UserService.findUserPermissionsByUsername = async (username) => {
  const user = (await UserRepository.findByUsername(username))[0];

  if (!user) {
    throw new NotFoundError('User not found');
  }

  const groups = await GroupRepository.findGroupsByNames(user.groups);

  const nameGroupPermissions = [];
  groups.map((it) => it.permissions.forEach((permission) => { nameGroupPermissions.push(permission); }));

  const groupPermissions = await PermissionRepository.findPermissionsByNames(nameGroupPermissions);

  const userPermissions = await PermissionRepository.findPermissionsByNames(user.permissions);

  const permissions = (
    userPermissions.map((it) => it.name)
      .concat(groupPermissions.map((it) => it.name))
  ).filter((value, index, self) => self.indexOf(value) === index);

  return ({
    groups: groups.map((it) => it.name),
    permissions,
  });
};

UserService.listUsersFromGroup = async (groupName) => {
  const group = (await GroupRepository.findByName(groupName))[0];

  if (!group) {
    throw new NotFoundError('Group Does not Exist');
  }

  const userGroup = await UserRepository.listUsersFromGroup(groupName);

  return userGroup;
};

UserService.addPermissionsToUser = async (username, permissionsNames) => {
  const user = (await UserRepository.findByUsername(username))[0];
  if (!user) {
    throw new NotFoundError('User not found');
  }
  const permissionsNotFound = [];

  await Promise.all(permissionsNames.map(async (name) => {
    if (user.permissions.includes(name)) {
      permissionsNotFound.push(name);
    } else {
      const nameFound = await PermissionRepository.findByName(name);

      if (nameFound[0]) {
        user.permissions.push(name);
      } else {
        permissionsNotFound.push(name);
      }
    }
  }));
  if (permissionsNotFound.length === permissionsNames.length) {
    throw new NotFoundError('Permissions Not Found or Already Added');
  }

  const message = await UserRepository.edit(user);

  if (permissionsNotFound.length > 0) {
    throw new PartialContentError(
      'The Following permissions could not be added because they '
            + `did not be found or they are already added: ${permissionsNotFound}`,
    );
  }

  return message;
};

UserService.removeGroupsToUser = async (username, groupsNames) => {
  const user = (await UserRepository.findByUsername(username))[0];
  if (!user) {
    throw new NotFoundError('User not found');
  }

  const groupsNotFound = groupsNames.filter((name) => !user.groups.includes(name));

  user.groups = user.groups.filter((name) => !groupsNames.includes(name));

  if (groupsNotFound.length === groupsNames.length) {
    throw new NotFoundError('Groups not found');
  }

  const message = await UserRepository.edit(user);

  if (groupsNotFound.length > 0) {
    throw new PartialContentError(
      'The following groups could not be removed from the '
            + `user because they did not be found: ${groupsNotFound}`,
    );
  }

  return message;
};

UserService.removePermission = async (codeUser, namePermission) => {
  const findUserByCode = (await UserRepository.findByCode(codeUser))[0];

  if (!findUserByCode) {
    throw new NotFoundError('User Not Founded');
  }

  const permissionToFind = findUserByCode.permissions.some((permission) => permission === namePermission);

  if (!permissionToFind) {
    throw new NotFoundError('Permission Not Founded In The User');
  }

  findUserByCode.permissions.splice((findUserByCode.permissions.indexOf(namePermission)), 1);

  await UserRepository.edit(findUserByCode);
};

UserService.activate = async (username) => {
  const user = (await UserRepository.findByUsername(username))[0];

  if (!user) {
    throw new NotFoundError('User does not exist');
  }

  if (user.isActive === true) {
    throw new BusinessError('User is already active');
  }

  UserRepository.activateByName(username);
};

UserService.desactivate = async (username) => {
  const user = (await UserRepository.findByUsername(username))[0];

  if (!user) {
    throw new NotFoundError('User does not exist');
  }

  if (user.isActive === false) {
    throw new BusinessError('User is already desactivated');
  }

  UserRepository.deactivateByName(username);
};
