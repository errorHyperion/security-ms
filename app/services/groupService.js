const GroupService = module.exports;

const { NotFoundError, PartialContentError, BusinessError } = require('../utils/ErrorHandlerMiddleware');
const GroupRepository = require('../repositories/groupRepository');
const PermissionRepository = require('../repositories/permissionRepository');
const UserRepository = require('../repositories/userRepository');

GroupService.addPermissionsToGroup = async (groupName, permissionsNames) => {
  const group = (await GroupRepository.findByName(groupName))[0];

  if (!group) {
    throw new NotFoundError('Group Not Found');
  }

  const permissionsNotFound = [];

  await Promise.all(permissionsNames.map(async (name) => {
    if (group.permissions.includes(name)) {
      permissionsNotFound.push(name);
    } else {
      const nameFound = await PermissionRepository.findByName(name);

      if (nameFound[0]) {
        group.permissions.push(name);
      } else {
        permissionsNotFound.push(name);
      }
    }
  }));

  if (permissionsNotFound.length === permissionsNames.length) {
    throw new NotFoundError('Permissions Not Found or Already Added');
  }

  const message = await GroupRepository.edit(group);

  if (permissionsNotFound.length > 0) {
    throw new PartialContentError(
      'The Following permissions could not be added because they '
            + `did not be found or they are already added: ${permissionsNotFound}`,
    );
  }

  return message;
};

GroupService.removePermissionsToGroup = async (groupName, permissionsNames) => {
  const group = (await GroupRepository.findByName(groupName))[0];

  if (!group) {
    throw new NotFoundError('Group Not Found');
  }

  const permissionsNotFound = permissionsNames.filter((name) => !group.permissions.includes(name));

  group.permissions = group.permissions.filter((name) => !permissionsNames.includes(name));

  if (permissionsNotFound.length === permissionsNames.length) {
    throw new NotFoundError('Permissions Not Found');
  }

  const message = await GroupRepository.edit(group);

  if (permissionsNotFound.length > 0) {
    throw new PartialContentError(
      'The Following permissions could not be removed from the '
            + `group because they did not be found: ${permissionsNotFound}`,
    );
  }

  return message;
};

GroupService.deleteGroup = async (groupName) => {
  const group = (await GroupRepository.findByName(groupName))[0];
  if (!group) {
    throw new NotFoundError('Group Does not Exist');
  }

  const usersGroup = await UserRepository.listUsersFromGroup(groupName);

  if (usersGroup.length > 0) {
    throw new BusinessError('A group with active users can not be delete');
  }

  await GroupRepository.delete(groupName);
};

GroupService.createGroup = async (group) => {
  const groupToFind = (await GroupRepository.findByName(group.name))[0];

  if (groupToFind) {
    throw new BusinessError('This group already exists');
  }

  await GroupRepository.create(group);
};

GroupService.listGroups = async () => {
  const listGroups = await GroupRepository.findAll();

  return listGroups;
};
