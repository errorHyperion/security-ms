const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  name: String,
  permissions: [String],
});

module.exports = mongoose.model('Group', schema, 'groups');
