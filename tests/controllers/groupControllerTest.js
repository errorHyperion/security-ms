const chai = require('chai');

const { assert } = chai;
const chaiHttp = require('chai-http');
const app = require('../../index');
const DBHelper = require('../DBHelper');
const GroupRepository = require('../../app/repositories/groupRepository');
const PermissionRepository = require('../../app/repositories/permissionRepository');
const UserRepository = require('../../app/repositories/userRepository');

const API = '/api/security-ms';
chai.use(chaiHttp);

describe('Add Permission to a Group Flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('Add all permissions sent to a group', async () => {
    const data = {
      permissions: [
        'editar_estudiante1',
      ],
    };
    const groupName = 'profesores1';

    await Promise.all([
      GroupRepository.create({ name: 'profesores1', permissions: [] }),
      PermissionRepository.create({ name: 'editar_estudiante1' }),
    ]);

    return chai
      .request(app)
      .put(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const group = await GroupRepository.findByName('profesores1');
        assert.isNotNull(group);
        assert.include(group[0].permissions, data.permissions[0]);
      });
  });

  it('Add permissions to an unexisting group', async () => {
    const data = {
      permissions: [
        'editar_estudiante',
      ],
    };
    const groupName = 'administrador';

    return chai
      .request(app)
      .put(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Group Not Found',
            code: 404,
          },
        });
      });
  });

  it('Add permissions that doesnt exist or are already added to the group', async () => {
    const data = {
      permissions: [
        'editar_estudiante2', // Already Added
        'buscar_profesor2', // Doesn't Exist
      ],
    };
    const groupName = 'profesores2';
    await Promise.all([
      PermissionRepository.create({ name: 'editar_estudiante2' }),
      GroupRepository.create({ name: 'profesores2', permissions: ['editar_estudiante2'] }),
    ]);

    return chai
      .request(app)
      .put(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Permissions Not Found or Already Added',
            code: 404,
          },
        });
      });
  });

  it('Add Permissions Partialy, Someones are already added,'
        + 'other ones does not exist and other ones can be added sucess', async () => {
    const data = {
      permissions: [
        'editar_estudiante3', // Already Added
        'buscar_profesor3', // Doesn't Exist
        'crear_estudiante3',
      ],
    };
    const groupName = 'profesores3';

    await Promise.all([
      GroupRepository.create({ name: 'profesores3', permissions: ['editar_estudiante3'] }),
      PermissionRepository.create({ name: 'editar_estudiante3' }),
      PermissionRepository.create({ name: 'crear_estudiante3' }),
    ]);

    return chai
      .request(app)
      .put(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 206);

        const group = await GroupRepository.findByName('profesores3');
        assert.isNotNull(group);
        assert.include(group[0].permissions, 'crear_estudiante3');
        assert.equal(group[0].permissions.length, 2);
      });
  });
});

describe('Revome Permission From a Group Flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('Remove all permissions sent to a group', async () => {
    const data = {
      permissions: [
        'editar_estudiante1',
      ],
    };
    const groupName = 'profesores1';

    await Promise.all([
      GroupRepository.create({ name: 'profesores1', permissions: ['editar_estudiante1'] }),
      PermissionRepository.create({ name: 'editar_estudiante1' }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const group = await GroupRepository.findByName('profesores1');
        assert.isNotNull(group);
        assert.equal(group[0].permissions.length, 0);
        assert.notInclude(group[0].permissions, 'editar_estudiante1');
      });
  });

  it('Remove permissions to an unexisting group', async () => {
    const data = {
      permissions: [
        'editar_estudiante',
      ],
    };
    const groupName = 'administradores';

    return chai
      .request(app)
      .delete(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Group Not Found',
            code: 404,
          },
        });
      });
  });

  it('Remove permissions that doesnt exist in the group', async () => {
    const data = {
      permissions: [
        'editar_estudiante2', // Doesn't Exist in the group
        'buscar_profesor2', // Doesn't Exist
      ],
    };
    const groupName = 'profesores2';

    await Promise.all([
      GroupRepository.create({ name: 'profesores2', permissions: [] }),
      PermissionRepository.create({ name: 'editar_estudiante2' }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Permissions Not Found',
            code: 404,
          },
        });
      });
  });

  it('Remove Permissions Partialy, Someones do not exist in the group,'
        + ' other ones does not exist and other ones can be removed sucess', async () => {
    const data = {
      permissions: [
        'editar_estudiante3', // Doesn't Exist in the Group
        'buscar_profesor3', // Doesn't Exist
        'crear_estudiante3',
      ],
    };
    const groupName = 'profesores3';

    await Promise.all([
      GroupRepository.create({ name: 'profesores3', permissions: ['crear_estudiante3'] }),
      PermissionRepository.create({ name: 'editar_estudiante3' }),
      PermissionRepository.create({ name: 'crear_estudiante3' }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/groups/${groupName}/permissions`)
      .send(data)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 206);

        const group = await GroupRepository.findByName('profesores3');
        assert.isNotNull(group);
        assert.equal(group[0].permissions.length, 0);
      });
  });
});

describe('Delete Group', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('Delete group', async () => {
    await GroupRepository.create({ name: 'teacher', permissions: ['permissionteacher1'] });

    return chai.request(app).delete(`${API}/groups/teacher`).then(async (response) => {
      const { status } = response;
      assert.strictEqual(status, 200);
      const groupToFind = GroupRepository.findByName('teacher');
      assert.isEmpty(groupToFind);
    });
  });

  it('Delete GroupNotExist', async () => {
    await GroupRepository.create({ name: 'teacher', permissions: ['permissionteacher1'] });

    return chai.request(app).delete(`${API}/groups/teacher11`).then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 404);
      assert.deepEqual(body, {
        error: {
          message: 'Group Does not Exist',
          code: 404,
        },
      });
    });
  });

  it('Delete Group with Users', async () => {
    await GroupRepository.create({ name: 'teacher', permissions: ['permissionteacher1'] });

    const data = {
      username: 'José Luis Hurtado',
      passwords: [{
        password: '123',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity',
      groups: ['teacher'],
      permissions: ['permissionstudent3'],
      mustChangePassword: false,
      isActive: true,
    };

    await UserRepository.create(data);

    return chai.request(app).delete(`${API}/groups/teacher`).then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 412);
      assert.deepEqual(body, {
        error: {
          message: 'A group with active users can not be delete',
          code: 412,
        },
      });
    });
  });
});

describe('List all students from a group', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('List users from a group', async () => {
    await GroupRepository.create({ name: 'teacher', permissions: ['permissionteacher1'] });
    const data = {
      username: 'José Luis Hurtado',
      passwords: [{
        password: '123',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity',
      groups: ['teacher'],
      permissions: ['permissionstudent3'],
      mustChangePassword: false,
      isActive: true,
    };

    await UserRepository.create(data);

    return chai
      .request(app).get(`${API}/groups/teacher/users`).then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 200);
        assert.equal(body[0].groups.length, 1);
        assert.equal(data.groups[0], body[0].groups[0]);
        assert.equal(data.username[0], body[0].username[0]);
      });
  });

  it('List users from a group', async () => {
    await GroupRepository.create({ name: 'teacher', permissions: ['permissionteacher1'] });
    const data = {
      username: 'José Luis Hurtado',
      passwords: [{
        password: '123',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity',
      groups: ['teacher'],
      permissions: ['permissionstudent3'],
      mustChangePassword: false,
      isActive: true,
    };

    await UserRepository.create(data);

    return chai
      .request(app).get(`${API}/groups/teacherssss/users`).then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);

        assert.deepEqual(body, {
          error: {

            message: 'Group Does not Exist',
            code: 404,

          },
        });
      });
  });
});

describe('Create group', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('create group', async () => chai.request(app).post(`${API}/groups`)
    .send({ name: 'grupo1', permissions: ['permiso1'] })
    .then(async (response) => {
      const { status } = response;
      assert.strictEqual(status, 200);

      const groupfind = await GroupRepository.findByName('grupo1');

      assert.isNotNull(groupfind);
      assert.equal(groupfind[0].name, 'grupo1');
    }));

  it('create group already exist', async () => {
    const data = {
      name: 'grupo2',
      permissions: ['permiso2'],
    };

    await GroupRepository.create(data);

    return chai
      .request(app)
      .post(`${API}/groups`)
      .send({ name: 'grupo2', permissions: ['permiso2'] })
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);

        assert.deepEqual(body, {
          error: {
            message: 'This group already exists',
            code: 412,
          },
        });
      });
  });
});

describe('List groups', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('List groups', async () => {
    await GroupRepository.create({ name: 'grupo 1', permissions: ['permiso1'] });
    await GroupRepository.create({ name: 'grupo 2', permissions: ['permiso1'] });
    await GroupRepository.create({ name: 'grupo 3', permissions: ['permiso1'] });
    await GroupRepository.create({ name: 'grupo 4', permissions: ['permiso1'] });

    return chai
      .request(app).get(`${API}/groups`).then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 200);
        assert.equal(4, body.length);
        assert.equal('grupo 1', body[0].name);
      });
  });
});
