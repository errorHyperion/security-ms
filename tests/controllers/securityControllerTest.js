const chai = require('chai');
const jwt = require('jsonwebtoken');

const { assert } = chai;
const chaiHttp = require('chai-http');
const app = require('../../index');
const DBHelper = require('../DBHelper');
const UserRepository = require('../../app/repositories/userRepository');

const {
  JWT_SECRET,
} = process.env;

const API = '/api/security-ms';
chai.use(chaiHttp);

describe('Login flows', () => {
  beforeEach(async () => { await DBHelper.clearAll(); });

  it('Login Successful', async () => {
    const userData = {
      username: 'sakamoto',
      email: 'jVillanueva@gmail.com',
      passwords: [{
        password: '$2b$12$019udfEgYJyNwznzAirtKeHxagz8adKa0ynrEV.EgcMFjeLxbL5B.',
        isCurrent: true,
      }],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    const data = {
      username: 'sakamoto',
      pass: 'pitenparoropompo',
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .post(`${API}/security/login`)
      .send(data)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 200);
        const user = jwt.verify(body.token, JWT_SECRET);
        assert.strictEqual(user.username, userData.username);
        assert.isEmpty(user.permissions);
        assert.isEmpty(user.groups);
      });
  });

  it('Login Wrong Username', async () => {
    const userData = {
      username: 'sakamoto1',
      email: 'jVillanueva1@gmail.com',
      passwords: [{
        password: '$2b$12$019udfEgYJyNwznzAirtKeHxagz8adKa0ynrEV.EgcMFjeLxbL5B.',
        isCurrent: true,
      }],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    const data = {
      username: 'sakamoto',
      pass: 'pitenparoropompo',
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .post(`${API}/security/login`)
      .send(data)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 403);
        assert.strictEqual(body.error.message, 'Invalid credentials');
      });
  });

  it('Login Wrong password', async () => {
    const userData = {
      username: 'sakamoto2',
      email: 'jVillanueva2@gmail.com',
      passwords: [{
        password: '$2b$12$019udfEgYJyNwznzAirtKeHxagz8adKa0ynrEV.EgcMFjeLxbL5B.',
        isCurrent: true,
      }],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    const data = {
      username: 'sakamoto',
      pass: '1',
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .post(`${API}/security/login`)
      .send(data)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 403);
        assert.strictEqual(body.error.message, 'Invalid credentials');
      });
  });
});

describe('Validation flows', () => {
  beforeEach(async () => { await DBHelper.clearAll(); });

  it('Validate a valid token', async () => {
    const payload = {
      username: 'sakamoto',
    };

    const token = await jwt.sign(payload, JWT_SECRET, { expiresIn: '3s' });

    const bodyToken = {
      token,
    };

    return chai
      .request(app)
      .post(`${API}/security/validate-token`)
      .send(bodyToken)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 200);
        assert.strictEqual(body.username, payload.username);
      });
  });

  it('Validate an invalid token', async () => {
    const payload = {
      username: 'sakamoto',
    };

    const token = jwt.sign(payload, JWT_SECRET, { expiresIn: '1s' });

    const bodyToken = {
      token,
    };

    const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
    await delay(1100);

    return chai
      .request(app)
      .post(`${API}/security/validate-token`)
      .send(bodyToken)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 403);
        assert.strictEqual(body.error.message, 'Invalid token');
      });
  });
});
