const chai = require('chai');

const { assert } = chai;
const chaiHttp = require('chai-http');
const app = require('../../index');
const DBHelper = require('../DBHelper');
const UserRepository = require('../../app/repositories/userRepository');
const GroupRepository = require('../../app/repositories/groupRepository');
const PermissionRepository = require('../../app/repositories/permissionRepository');

const API = '/api/security-ms';
chai.use(chaiHttp);

describe('User CRUD flows', () => {
  beforeEach(async () => { await DBHelper.clearAll(); });

  it('create user validation success', async () => chai
    .request(app)
    .post(`${API}/users`)
    .send({
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    })
    .then(async (response) => {
      const { status } = response;
      assert.strictEqual(status, 200);

      const user = await UserRepository.findByUsername('felipe');

      assert.isNotNull(user);
      assert.equal(user[0].username, 'felipe');
    }));

  it('create user already exist', async () => {
    const data = {
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    await UserRepository.create(data);

    return chai
      .request(app)
      .post(`${API}/users`)
      .send({
        username: 'felipe',
        email: 'felipe@gmail.com',
        passwords: [],
        groups: [],
        permissions: [],
        mustChangePassword: true,
        isActive: true,
      })
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);

        assert.deepEqual(body, {
          error: {
            message: 'Username is repeated.',
            code: 412,
          },
        });
      });
  });

  it('create user with wrong mail', async () => chai
    .request(app)
    .post(`${API}/users`)
    .send({
      username: 'felipe',
      email: 'dadwad',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    })
    .then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 412);

      assert.deepEqual(body, {
        error: {
          message: 'Email is not valid.',
          code: 412,
        },
      });
    }));

  it('add group to use', async () => {
    const userData = {
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };
    await UserRepository.create(userData);

    const groupData = {
      name: 'groupA',
      permissions: [],
    };
    await GroupRepository.create(groupData);

    return chai
      .request(app)
      .put(`${API}/users/${userData.username}/update/groups/${groupData.name}`)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);
        const user = await UserRepository.findByUsername('felipe');
        assert.equal(user[0].groups[0], 'groupA');
      });
  });

  it('add group to user and user does not exist', async () => {
    const userData = {
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    const groupData = {
      name: 'groupA',
      permissions: [],
    };
    await GroupRepository.create(groupData);

    return chai
      .request(app)
      .put(`${API}/users/${userData.username}/update/groups/${groupData.name}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User does not exist.',
            code: 404,
          },
        });
      });
  });

  describe('Change user password flows', () => {
    beforeEach(async () => {
      await DBHelper.clearAll();
    });

    it('Change user password sucess', async () => {
      const data = {
        username: 'pabloGuzman1',
        passwords: [{
          password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G', // 000000001
          createdAt: new Date(),
          validUntil: new Date(),
          isCurrent: true,
        }],
        code: '1',
        entity: 'Entity',
        groups: [],
        permissions: [],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .put(`${API}/users/${user.username}/change-password`)
        .send({
          currentPassword: '000000001',
          newPassword: '000000002',
        })
        .then(async (response) => {
          const { status } = response;
          assert.strictEqual(status, 200);

          const userToAssert = (await UserRepository.findByUsername('pabloGuzman1'))[0];

          assert.isNotNull(userToAssert);
          assert.equal(userToAssert.username, 'pabloGuzman1');
          assert.equal(userToAssert.passwords.length, 2);
          assert.equal(userToAssert.passwords[0].isCurrent, true);
          assert.equal(userToAssert.passwords[1].isCurrent, false);
        });
    });

    it('Change user password when the password is invalid', async () => {
      const data = {
        username: 'pabloGuzman1',
        passwords: [{
          password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G', // 000000001
          createdAt: new Date(),
          validUntil: new Date(),
          isCurrent: true,
        }],
        code: '1',
        entity: 'Entity',
        groups: [],
        permissions: [],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .put(`${API}/users/${user.username}/change-password`)
        .send({
          currentPassword: '000000001',
          newPassword: '1',
        })
        .then(async (response) => {
          const { body, status } = response;
          assert.strictEqual(status, 412);

          assert.deepEqual(body, {
            error: {
              message: 'The password must be at least 8 characters long',
              code: 412,
            },
          });
        });
    });

    it('Change user password when user does not exist', async () => chai
      .request(app)
      .put(`${API}/users/pablo/change-password`)
      .send({
        currentPassword: '000000001',
        newPassword: '000000002',
      })
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);

        assert.deepEqual(body, {
          error: {
            message: 'User not found',
            code: 404,
          },
        });
      }));

    it('Change user password when the current password is incorrect', async () => {
      const data = {
        username: 'pabloGuzman2',
        passwords: [{
          password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G', // 000000001
          createdAt: new Date(),
          validUntil: new Date(),
          isCurrent: true,
        }],
        code: '1',
        entity: 'Entity',
        groups: [],
        permissions: [],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .put(`${API}/users/${user.username}/change-password`)
        .send({
          currentPassword: '0000000011',
          newPassword: '000000002',
        })
        .then(async (response) => {
          const { body, status } = response;
          assert.strictEqual(status, 412);

          assert.deepEqual(body, {
            error: {
              message: 'The current password is incorrect',
              code: 412,
            },
          });
        });
    });

    it('Change user password when the new password has already been used before', async () => {
      const data = {
        username: 'pabloGuzman3',
        passwords: [
          {
            password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G', // 000000001
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: true,
          },
          {
            password: '$2b$12$mK6232JBTSDpLo9TEEaIseC3JQLtjSBDl1KEPGaOi8Zaz4aOofRuO', // 000000002
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
        ],
        code: '1',
        entity: 'Entity',
        groups: [],
        permissions: [],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .put(`${API}/users/${user.username}/change-password`)
        .send({
          currentPassword: '000000001',
          newPassword: '000000002',
        })
        .then(async (response) => {
          const { body, status } = response;
          assert.strictEqual(status, 412);

          assert.deepEqual(body, {
            error: {
              message: 'The new password has already been used before',
              code: 412,
            },
          });
        });
    });

    it('Change user password when new password is reused after ten passwords', async () => {
      const data = {
        username: 'pabloGuzman4',
        passwords: [
          {
            password: '$2b$12$M1nWxteE4H9syGlfKzAjEOqYx6IQHzoxY9lROKkXcN5Sb3QtjJ7Fm', // 000000001
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: true,
          },
          {
            password: '$2b$12$xLlnjZjAuqAkVJ359klg.u3lU4MKSwxUyFozMTpffS7D7Ueu/Bzcu', // 000000002
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$nfAsCXlxQ5KFP41JqhJMduifxfCumHnDYtDppU0C3vMGzHeutk1be', // 000000003
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$7o5ICHAUXPLvZkNqotMNSehYy.Zn2d8Mo8n7g18x5ysPL/Wm5vSiC', // 000000004
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$Q3p5G/u7JAMNT.pvMPGF9OQDcyMuDuXk0tuTkRJZiZ666qRvpQgqe', // 000000005
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$udD.Jc6dv9K2zrjmzZAjxehfcAoqzz6DXAKU3AvkFxxSmRLJY7J7C', // 000000006
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$YvE6Hp50KjIEDvFlH/Vv9uv1Bf/dnrKeJsDmtJoXdmOmBtu9jCzGK', // 000000007
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$2ROSgNPFnlE2M7YGhAz7suP9OJYz6zfa5ohdj8.K0MN0GdsN3UH5y', // 000000008
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$/2oBL.7AZX/QOWok0B9Lke1OJ2EpHN.y89pCk8fgqJEbmkv4JPpFm', // 000000009
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
          {
            password: '$2b$12$zCBaQ.UthCAgi/cZzhgaXOy09.kRX/G89UsfvFOfwwFlIkzo1Jcfy', // 000000010
            createdAt: new Date(),
            validUntil: new Date(),
            isCurrent: false,
          },
        ],
        code: '1',
        entity: 'Entity',
        groups: [],
        permissions: [],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .put(`${API}/users/${user.username}/change-password`)
        .send({
          currentPassword: '000000001',
          newPassword: '000000010',
        })
        .then(async (response) => {
          const { status } = response;
          assert.strictEqual(status, 200);

          const userToAssert = (await UserRepository.findByUsername(data.username))[0];

          assert.isNotNull(userToAssert);
          assert.equal(userToAssert.username, 'pabloGuzman4');
          assert.equal(userToAssert.passwords.length, 10);
          assert.equal(userToAssert.passwords[1].isCurrent, false);
          assert.equal(userToAssert.passwords[0].isCurrent, true);
        });
    });
  });

  describe('Find user permissions by username flows', () => {
    beforeEach(async () => {
      await DBHelper.clearAll();
    });

    it('Find user permissions by username success', async () => {
      await PermissionRepository.create({ name: 'permissionteacher1' });
      await PermissionRepository.create({ name: 'permissionadmin2' });
      await PermissionRepository.create({ name: 'permissionstudent3' });

      await GroupRepository.create({ name: 'teacher1', permissions: ['permissionteacher1'] });
      await GroupRepository.create({ name: 'admin2', permissions: ['permissionadmin2'] });
      await GroupRepository.create({ name: 'student3', permissions: ['permissionstudent3'] });

      const data = {
        username: 'pabloGuzman5',
        passwords: [{
          password: '123',
          createdAt: new Date(),
          validUntil: new Date(),
          isCurrent: true,
        }],
        code: '1',
        entity: 'Entity',
        groups: ['teacher1', 'admin2', 'student3'],
        permissions: ['permissionstudent3'],
        mustChangePassword: false,
        isActive: true,
      };

      const user = await UserRepository.create(data);

      return chai
        .request(app)
        .get(`${API}/users/${user.username}/authorizations`)
        .then(async (response) => {
          const { body, status } = response;

          assert.strictEqual(status, 200);
          assert.equal(body.groups.length, 3);
          assert.equal(data.groups[0], body.groups[0]);
          assert.equal(data.groups[1], body.groups[1]);
          assert.equal(data.groups[2], body.groups[2]);
          assert.equal(body.permissions.length, 3);
          assert.equal('permissionstudent3', body.permissions[0]);
          assert.equal('permissionteacher1', body.permissions[1]);
          assert.equal('permissionadmin2', body.permissions[2]);
        });
    });

    it('Find user permissions when user does not exist', async () => chai
      .request(app)
      .get(`${API}/users/pabloGuzman6/authorizations`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);

        assert.deepEqual(body, {
          error: {
            message: 'User not found',
            code: 404,
          },
        });
      }));
  });

  it('changing is active to true', async () => {
    const userData = {
      username: 'juan',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: false,
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/activate`)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);
        const user = await UserRepository.findByUsername('juan');
        assert.isNotNull(user);
        assert.equal(user[0].username, 'juan');
        assert.equal(user[0].isActive, true);
      });
  });

  it('changing is active when is active is true', async () => {
    const userData = {
      username: 'Jeronimo',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/activate`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'User is already active',
            code: 412,
          },
        });
      });
  });

  it('changing is active to true when user does not exist', async () => {
    const userData = {
      username: 'Jeronimo',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/activate`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User does not exist',
            code: 404,
          },
        });
      });
  });

  it('changing is active to false', async () => {
    const userData = {
      username: 'Jeronimo',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/desactivate`)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);
        const user = await UserRepository.findByUsername('Jeronimo');
        assert.isNotNull(user);
        assert.equal(user[0].username, 'Jeronimo');
        assert.equal(user[0].isActive, false);
      });
  });

  it('changing is active when is active is false', async () => {
    const userData = {
      username: 'Jeronimo',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: false,
    };

    await UserRepository.create(userData);

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/desactivate`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'User is already desactivated',
            code: 412,
          },
        });
      });
  });

  it('changing is active to false when user does not exist', async () => {
    const userData = {
      username: 'Jeronimo',
      email: 'jeronimo@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };

    return chai
      .request(app)
      .put(`${API}/user/${userData.username}/desactivate`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User does not exist',
            code: 404,
          },
        });
      });
  });
});

describe('Add permission to user flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('Add all permissions sent to a user', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: [],
      permissions: [],
      mustChangePassword: false,
      isActive: true,
    };
    const permissionToTest = {
      permissions: [
        'Permiso_1',
      ],
    };
    await Promise.all([
      UserRepository.create(data),
      PermissionRepository.create({ name: 'Permiso_1' }),
    ]);

    return chai
      .request(app)
      .put(`${API}/users/cesar/permissions`)
      .send(permissionToTest)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const user = await UserRepository.findByUsername('cesar');
        assert.isNotNull(user);
        assert.include(user[0].permissions, permissionToTest.permissions[0]);
      });
  });

  it('Add permissions to an unexisting user', async () => {
    const permissionToTest = {
      permissions: [
        'Permiso_1',
      ],
    };
    const username = 'ramon123';

    return chai
      .request(app)
      .put(`${API}/users/${username}/permissions`)
      .send(permissionToTest)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User not found',

            code: 404,
          },
        });
      });
  });

  it('add group to user and group does not exist', async () => {
    const userData = {
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };
    await UserRepository.create(userData);
    const groupData = {
      name: 'groupA',
      permissions: [],
    };

    return chai
      .request(app)
      .put(`${API}/users/${userData.username}/update/groups/${groupData.name}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'There is not a group with this name.',
            code: 404,
          },
        });
      });
  });

  it('Add permissions that does not exist or are already added to user', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: [],
      permissions: ['Permiso_1'],
      mustChangePassword: false,
      isActive: true,
    };
    const permissionToTest = {
      permissions: [
        'Permiso_1',
        'Permiso_2',
      ],
    };
    await Promise.all([
      UserRepository.create(data),
      PermissionRepository.create({ name: 'Permiso_1' }),
    ]);

    return chai
      .request(app)
      .put(`${API}/users/cesar/permissions`)
      .send(permissionToTest)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Permissions Not Found or Already Added',

            code: 404,
          },
        });
      });
  });

  it('add group to user and user already has it', async () => {
    const userData = {
      username: 'felipe',
      email: 'felipe@gmail.com',
      passwords: [],
      groups: ['groupA'],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };
    await UserRepository.create(userData);

    const groupData = {
      name: 'groupA',
      permissions: [],
    };
    await GroupRepository.create(groupData);

    return chai
      .request(app)
      .put(`${API}/users/${userData.username}/update/groups/${groupData.name}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'User has this group already.',
            code: 412,
          },
        });
      });
  });

  it('Add permissions, someones are already added,'
    + 'others ones does not exist and other ones can be added sucess', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: [],
      permissions: ['Permiso_1'],
      mustChangePassword: false,
      isActive: true,
    };
    const permissionToTest = {
      permissions: [
        'Permiso_1',
        'Permiso_2',
        'Permiso_3',
      ],
    };
    await Promise.all([
      UserRepository.create(data),
      PermissionRepository.create({ name: 'Permiso_1' }),
      PermissionRepository.create({ name: 'Permiso_2' }),
    ]);

    return chai
      .request(app)
      .put(`${API}/users/cesar/permissions`)
      .send(permissionToTest)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 206);

        const user = await UserRepository.findByUsername('cesar');
        assert.isNotNull(user);
        assert.include(user[0].permissions, 'Permiso_1');
        assert.equal(user[0].permissions.length, 2);
      });
  });
});

describe('Remove group from a user flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('Remove groups sent to a user', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: ['Group_1'],
      permissions: [],
      mustChangePassword: false,
      isActive: true,
    };
    const groupsToTest = {
      groups: ['Group_1'],
    };

    await Promise.all([
      UserRepository.create(data),
      GroupRepository.create({ name: 'Group_1', permissions: [] }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/users/cesar/groups`)
      .send(groupsToTest)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const user = await UserRepository.findByUsername('cesar');
        assert.isNotNull(user);
        assert.equal(user[0].groups.length, 0);
        assert.notInclude(user[0].groups, 'Group_1');
      });
  });

  it('Remove groups to an unexisting user', async () => {
    const groupsToTest = {
      name: 'Group_1',
      permissions: [],
    };

    return chai
      .request(app)
      .delete(`${API}/users/user1/groups`)
      .send(groupsToTest)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User not found',
            code: 404,
          },
        });
      });
  });

  it('Remove groups that does not exist in the user', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: [],
      permissions: [],
      mustChangePassword: false,
      isActive: true,
    };
    const groupsToTest = {
      groups: ['Group_1'],
    };

    await Promise.all([
      UserRepository.create(data),
      GroupRepository.create({ name: 'Group_1', permissions: [] }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/users/cesar/groups`)
      .send(groupsToTest)
      .then(async (response) => {
        const { status, body } = response;
        assert.strictEqual(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Groups not found',
            code: 404,
          },
        });
      });
  });

  it('Remove groups partialy, someones do not exist in the user'
    + ' other ones does not exist and other ones can be removed sucess', async () => {
    const data = {
      username: 'cesar',
      passwords: [{
        password: '$2b$12$o9dff7z6Bx8UiaNt/2GA3ugc919FpJ3QnHAllE69VRx/NKeGQgA7G',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '1',
      entity: 'Entity1',
      groups: ['group_1'],
      permissions: [],
      mustChangePassword: false,
      isActive: true,
    };
    const groupsToTest = {
      groups: [
        'group_1',
        'group_2',
        'group_3',
      ],
    };

    await Promise.all([
      UserRepository.create(data),
      GroupRepository.create({ name: 'group_1', permissions: [] }),
      GroupRepository.create({ name: 'group_2', permissions: [] }),
    ]);

    return chai
      .request(app)
      .delete(`${API}/users/cesar/groups`)
      .send(groupsToTest)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 206);

        const user = await UserRepository.findByUsername('cesar');
        assert.isNotNull(user);
        assert.equal(user[0].groups.length, 0);
      });
  });
});

describe('User DELETE permission flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('delete permission from user validation success', async () => {
    const data = {
      username: 'sebitas',
      email: 'sebas@gmail.com',
      passwords: [{
        password: '12345',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '100',
      entity: 'entity',
      groups: ['grupopermiso', 'grupoprofesor'],
      permissions: ['permisoacademico', 'permisonotas', 'permisotemporal'],
      mustChangePassword: false,
      isActive: true,
    };
    const user = await UserRepository.create(data);
    const permiso = 'permisoacademico';

    return chai
      .request(app)
      .delete(`${API}/users/${user.code}/permission/${permiso}`)
      .then(async (response) => {
        const { status } = response;
        assert.equal(status, 200);
        const onlyUser = await UserRepository.findByCode(data.code);
        assert.equal(onlyUser[0].permissions.length, 2);
      });
  });

  it('delete permission from user validation user not found', async () => {
    const code = '9283';
    const permission = 'permiso';

    return chai
      .request(app)
      .delete(`${API}/users/${code}/permission/${permission}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.equal(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'User Not Founded',
            code: 404,
          },
        });
      });
  });

  it('delete permission fromuser validation permission not found', async () => {
    const data = {
      username: 'sebitas',
      email: 'sebas@gmail.com',
      passwords: [{
        password: '12345',
        createdAt: new Date(),
        validUntil: new Date(),
        isCurrent: true,
      }],
      code: '100',
      entity: 'entity',
      groups: ['grupopermiso', 'grupoprofesor'],
      permissions: ['permisoacademico', 'permisonotas', 'permisotemporal'],
      mustChangePassword: false,
      isActive: true,
    };
    const user = await UserRepository.create(data);
    const permiso = 'permisonovalido';

    return chai
      .request(app)
      .delete(`${API}/users/${user.code}/permission/${permiso}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.equal(status, 404);
        assert.deepEqual(body, {
          error: {
            message: 'Permission Not Founded In The User',
            code: 404,
          },
        });
      });
  });
});
